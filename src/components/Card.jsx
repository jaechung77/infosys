import React, { useState } from 'react';
import './styles/Employees.css';

const Card = ({ employee, handleModal }) => {
  const { id, avatar, firstName, contactNo, lastName, address } = employee;
  const [isActive, setIsActive] = useState(false);
  const FALLBACK_IMAGE = 'noImage.jpeg';
  const imageOnErrorHandler = (event) => {
    event.currentTarget.src = FALLBACK_IMAGE;
    event.currentTarget.className = 'error';
  };

  const handleClick = () => {
    handleModal(id);
    setIsActive(!isActive);
  };

  return (
    <li
      className="table-row"
      style={{
        backgroundColor: isActive ? 'salmon' : 'white',
        color: isActive ? 'white' : 'black',
      }}
      onClick={handleClick}
    >
      <div className="col col-1" data-label="ID">
        {id}
      </div>
      <div className="col col-2" data-label="Name">
        <div className="img-box">
          <img
            className="image"
            src={avatar}
            onError={imageOnErrorHandler}
            width="100px"
            heigh="100px"
          />

          <span className="name">
            {firstName} {lastName}
          </span>
        </div>
      </div>
      <div className="col col-3" data-label="Contact No.">
        {contactNo}
      </div>
      <div className="col col-4" data-label="Address">
        {address}
      </div>
    </li>
  );
};

export default Card;
