import React from 'react';
import './styles/Search.css';

const Search = ({ postsPerPage, howManyEmployees }) => {
  return (
    <div className="search">
      <div>
        <span>
          Showing {postsPerPage} of {howManyEmployees}
        </span>
      </div>
      <div>
        <input type="text" className="search-box" placeholder="Search..." />
        <button
          className="button"
          onClick={() => {
            alert('This function does not meet the requirements!!');
          }}
        >
          Search
        </button>
      </div>
    </div>
  );
};

export default Search;
