import React from 'react';
import Card from './Card';

function Employees({
  employees,
  loading,
  error,
  handleModal,
  handleSelection,
  resetCard,
}) {
  return (
    <>
      {loading && <p>Loading...</p>}
      <ul className="responsive-table">
        <li className="table-header">
          <div className="col col-1">ID</div>
          <div className="col col-2">Name</div>
          <div className="col col-3">Contact No.</div>
          <div className="col col-4">Address</div>
        </li>
        {employees &&
          employees.map((employee, idx) => (
            <Card
              key={idx}
              employee={employee}
              handleModal={handleModal}
              handleSelection={handleSelection}
              resetCard={resetCard}
            />
          ))}
      </ul>
      {error && !loading && <p>{error}</p>}
    </>
  );
}

export default Employees;
