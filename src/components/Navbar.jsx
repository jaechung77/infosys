import React from 'react';
import './styles/Navbar.css';

export const Navbar = ({ companyName, companyMotto, estDate }) => {
  return (
    <header className="header">
      <div className="logo">{companyName}</div>
      <div className="sub-header">
        <span>{companyMotto}</span>
        <span>since {estDate}</span>
      </div>
    </header>
  );
};

export default Navbar;
