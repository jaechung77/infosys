import React from 'react';
import { createPortal } from 'react-dom';
import './styles/Modal.css';

const Modal = ({ isModalOn, onClose, selectedID, employees }) => {
  if (!isModalOn) {
    return null;
  }

  const employee =
    employees && employees.find((employee) => employee.id === selectedID);
  const FALLBACK_IMAGE = 'noImage.jpeg';
  const imageOnErrorHandler = (event) => {
    event.currentTarget.src = FALLBACK_IMAGE;
    event.currentTarget.className = 'error';
  };
  return createPortal(
    <>
      <div
        className={`overlay ${isModalOn ? 'show' : ''} `}
        onClick={() => onClose()}
      >
        <div className="modal" onClick={(e) => e.stopPropagation()}>
          <span className="close" onClick={onClose}>
            X
          </span>
          <div className="content">
            <div className="modal-left">
              <div>
                <img
                  className="image"
                  src={employee.avatar}
                  onError={imageOnErrorHandler}
                  width="100px"
                  heigh="100px"
                />
              </div>
              <div>{employee.jobTitle}</div>
              <div>{employee.age}</div>
              <div>{employee.dateJoined.slice(0, 10)}</div>
            </div>
            <div className="modal-right">
              <div className="title">
                {employee.firstName} {employee.lastName}
              </div>
              <div className="bio">{employee.bio}</div>
            </div>
          </div>
        </div>
      </div>
    </>,
    document.getElementById('modal')
  );
};

export default Modal;
