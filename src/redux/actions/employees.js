import * as type from '../types';

export const getCompanyEmployees = () => {
  return {
    type: type.GET_EMPLOYEES_REQUESTED,
  };
};
