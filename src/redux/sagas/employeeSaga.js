import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';

const apiEndPoint = 'sample-data.json';

async function getApi() {
  try {
    const response = await axios.get(apiEndPoint);
    return response.data;
  } catch (error) {
    throw error;
  }
}

function* fetchEmployees() {
  try {
    const employees = yield call(getApi);

    yield put({
      type: 'GET_EMPLOYEES_SUCCESS',
      employees: employees,
    });
  } catch (e) {
    yield put({ type: 'GET_EMPLOYEES_FAILURE', message: e.message });
  }
}

function* employeeSaga() {
  yield takeEvery('GET_EMPLOYEES_REQUESTED', fetchEmployees);
}

export default employeeSaga;
