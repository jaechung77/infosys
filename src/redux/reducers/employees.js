import * as type from '../types';

const initialState = {
  employees: {
    companyInfo: {
      companyName: '',
      companyMotto: '',
      companyEst: '',
    },
  },
  loading: false,
  error: null,
};

export function companyEmployees(state = initialState, action) {
  switch (action.type) {
    case type.GET_EMPLOYEES_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case type.GET_EMPLOYEES_SUCCESS:
      return {
        ...state,
        employees: action.employees,
        loading: false,
      };
    case type.GET_EMPLOYEES_FAILURE:
      return {
        ...state,
        locading: false,
        error: action.message,
      };
    default:
      return state;
  }
}
