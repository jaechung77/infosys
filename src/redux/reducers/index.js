import { combineReducers } from 'redux';
import { companyEmployees } from './employees';

const rootReducer = combineReducers({
  companyEmployees,
});

export default rootReducer;
