import { useSelector, useDispatch } from 'react-redux';
import React, { useState, useEffect } from 'react';
import { getCompanyEmployees } from './redux/actions/employees';
import './components/styles/App.css';
import Employees from './components/Employees';
import Navbar from './components/Navbar';
import Modal from './components/Modal';
import Pagination from './components/Pagination';
import Search from './components/Search';

function App() {
  const dispatch = useDispatch();
  const employees = useSelector(
    (state) => state.companyEmployees.employees.employees
  );
  const loading = useSelector((state) => state.companyEmployees.loading);
  const error = useSelector((state) => state.companyEmployees.error);
  const companyName = useSelector(
    (state) => state.companyEmployees.employees.companyInfo.companyName
  );
  const companyMotto = useSelector(
    (state) => state.companyEmployees.employees.companyInfo.companyMotto
  );
  const companyEst = useSelector(
    (state) => state.companyEmployees.employees.companyInfo.companyEst
  );
  const [isModalOn, setIsModalOn] = useState(false);
  const [resetCard, setResetCard] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(5);
  const [selectedID, setSelectedID] = useState('');
  const [key, setKey] = useState(1);

  const handleModal = (id) => {
    setIsModalOn(!isModalOn);
    setSelectedID(id);
    setResetCard(!resetCard);
  };

  const modalClose = () => {
    setIsModalOn(false);
    setKey(key + 1);
  };

  useEffect(() => {
    dispatch(getCompanyEmployees());
  }, []);

  // Pagination
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts =
    employees && employees.slice(indexOfFirstPost, indexOfLastPost);
  const howManyPages = employees && Math.ceil(employees.length / postsPerPage);
  const howManyEmployees = employees && employees.length;

  const estDate = employees && companyEst.slice(0, 10);

  return (
    <div className="container">
      <Navbar
        className="navbar"
        companyName={companyName}
        companyMotto={companyMotto}
        estDate={estDate}
      />
      <Search
        className="search"
        postsPerPage={postsPerPage}
        howManyEmployees={howManyEmployees}
      />
      {employees && (
        <Employees
          employees={currentPosts}
          loading={loading}
          error={error}
          handleModal={handleModal}
          resetCard={resetCard}
          key={key}
        />
      )}
      {employees && (
        <Pagination pages={howManyPages} setCurrentPage={setCurrentPage} />
      )}
      <Modal
        selectedID={selectedID}
        employees={employees}
        isModalOn={isModalOn}
        onClose={modalClose}
      />
    </div>
  );
}

export default App;
